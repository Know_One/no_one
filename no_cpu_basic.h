/*
 * Copyright: (C) 2016 Douglas Ballard III
 * Version: 1.0
 * License: Creative Commons Attribution 4.0 International Public License
 *          http://creativecommons.org/licenses/by/4.0/
 *          http://creativecommons.org/licenses/by/4.0/legalcode
 */

#ifndef NO_ONE_CPU_BASIC_H
#define NO_ONE_CPU_BASIC_H 1

#if defined(__alpha__) || defined(__alpha) || defined(_M_ALPHA)
# define NO_BI_ENDIAN 1
# define NO_LITTLE_ENDIAN 1 /* Unless you're running a Cray, it's LE */
# define NO_NATIVE_64_BIT 1
# define NO_CPU "Alpha"
# define NO_ARCH "Alpha"
# define NO_IS_ALPHA 1
#endif

#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_X64) || defined(_M_AMD64)
# define NO_NATIVE_64_BIT 1
# define NO_CPU "x86"
# define NO_ARCH "x86_64"
# define NO_LITTLE_ENDIAN 1
# define NO_IS_X86 1
# define NO_IS_X86_64 1
# define NO_IS_AMD64 1
#endif

#if defined(__arm__) || defined(__thumb__) || defined(__TARGET_ARCH_ARM) || defined(__TARGET_ARCH_THUMB) || defined(_ARM) || defined(_M_ARM) || defined(_M_ARMT) || defined(__arm) || defined(__aarch64__)
# define NO_BI_ENDIAN 1
# if defined(__aarch64__)
#  define NO_NATIVE_64_BIT 1
# elif defined(__thumb__) || defined(__TARGET_ARCH_THUMB)
#  define NO_NATIVE_16_BIT 1
# define NO_IS_THUMB 1
# else
#  define NO_NATIVE_32_BIT 1
# endif
# define NO_CPU "Arm"
# if defined(__ARM_ARCH_2__)
#  define NO_ARCH "Arm_2"
# elif defined(__ARM_ARCH_3__) || defined(__ARM_ARCH_3M__)
#  define NO_ARCH "Arm_3"
# elif defined(__ARM_ARCH_4T__) || defined(__TARGET_ARM_4T)
#  define NO_ARCH "Arm_4"
# elif defined(__ARM_ARCH_5__) || defined(__ARM_ARCH_5E__)
#  define NO_ARCH "Arm_5"
# elif defined(__ARM_ARCH_5T__) || defined(__ARM_ARCH_5TE__) || defined(__ARM_ARCH_5TEJ__)
#  define NO_ARCH "Arm_5T"
# elif defined(__ARM_ARCH_6__) || defined(__ARM_ARCH_6J__) || defined(__ARM_ARCH_6K__) || defined(__ARM_ARCH_6Z__) || defined(__ARM_ARCH_6ZK__)
#  define NO_ARCH "Arm_6"
# elif defined(__ARM_ARCH_6T2__)
#  define NO_ARCH "Arm_6T"
# elif defined(__ARM_ARCH_7__) || defined(__ARM_ARCH_7A__) || defined(__ARM_ARCH_7R__) || defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7S__)
#  define NO_ARCH "Arm_7"
# elif defined(__ARM_ARCH_8__) /* Need more info for arm 8 */
#  define NO_ARCH "Arm_8"
# else
#  define NO_ARCH "Arm"
# endif
# if defined(__ARMEB__) || defined(__THUMBEB__) || defined(__AARCH64EB__)
#  define NO_BIG_ENDIAN 1
# else
#  define NO_LITTLE_ENDIAN 1
# endif
# define NO_IS_ARM 1
#endif

#if defined(__bfin) || defined(__BFIN__)
# define NO_NATIVE_32_BIT 1
# define NO_CPU "BlackFin"
# define NO_ARCH "BlackFin"
# define NO_LITTLE_ENDIAN 1
# define NO_IS_BLACKFIN 1
#endif

#if defined(__epiphany__)
# define NO_NATIVE_32_BIT 1
# define NO_CPU "Epiphany"
# define NO_ARCH "Epiphany"
# define NO_LITTLE_ENDIAN 1
# define NO_IS_EPIPHANY 1
#endif

#if defined(__hppa__) || defined(__HPPA__) || defined(__hppa)
# define NO_BI_ENDIAN 1
# define NO_CPU "Hp-PA/RISC"
#  define NO_BIG_ENDIAN 1
# if defined(_PA_RISC2_0) || defined(__RISC2_0__) || defined(__HPPA20__) || defined(__PA8000__)
#  define NO_ARCH "PA/RISC_2"
#  define NO_NATIVE_64_BIT 1
# else
#  define NO_NATIVE_32_BIT 1
# endif
# if defined(_PA_RISC1_1) || defined(__HPPA11__) || defined(__PA7100__) 
#  define NO_ARCH "PA/RISC_1.1"
# endif
# if defined(_PA_RISC2_0)
#  define NO_ARCH "PA/RISC_1"
# endif
# define NO_IS_HPPA 1
#endif

#if defined(i386) || defined(__i386) || defined(__i386__) || defined(__IA32__) || defined(_M_I86) || defined(_M_IX86) || defined(__X86__) || defined(_X86_) || defined(__THW_INTEL__) || defined(__I86__) || defined(__INTEL__) || defined(__386)
# if defined(_M_I86)
#  define NO_NATIVE_16_BIT 1
# else
#  define NO_NATIVE_32_BIT 1
# endif
# define NO_CPU "x86"
# if defined(__i386__) || __I86__ == 3 || _M_IX86 == 300
#  define NO_ARCH "i386"
#  define NO_IS_I386 1
# elif defined(__i486__) || __I86__ == 4 || _M_IX86 == 400
#  define NO_ARCH "i486"
#  define NO_IS_I486 1
# elif defined(__i586__) || __I86__ == 5 || _M_IX86 == 500
#  define NO_ARCH "i586"
#  define NO_IS_I586 1
# elif defined(__i686__) || __I86__ == 6 || _M_IX86 == 600
#  define NO_ARCH "i686"
#  define NO_IS_I686 1
# endif
# define NO_LITTLE_ENDIAN 1
# define NO_IS_X86 1
# define NO_IS_X86_32 1
#endif

#if defined(__ia64__) || defined(_IA64) || defined(__IA64__) || defined(__ia64) || defined(_M_IA64) || defined(__itanium__)
# define NO_BI_ENDIAN 1
# define NO_SELECTABLE_ENDIAN 1
# define NO_NATIVE_64_BIT 1
# define NO_CPU "Itanium"
# define NO_ARCH "IA-64"
# if define(_hpux) || define(hpux) || define(__hpux)
#  define NO_BIG_ENDIAN 1
# else
#  define NO_LITTLE_ENDIAN 1
# endif
# define NO_IS_ITANIUM 1
#endif

#if defined(__m68k__) || defined(M68000) || defined(__MC68K__)
# define NO_NATIVE_32_BIT 1
# define NO_CPU "68K"
# if defined(__mc68000__) || defined(__mc68000__)
#  define NO_ARCH "68000k"
# elif defined(__mc68010__)
#  define NO_ARCH "68010k"
# elif defined(__mc68020__) || defined(__MC68020__)
#  define NO_ARCH "68020k"
# elif defined(__mc68030__) || defined(__MC68030__)
#  define NO_ARCH "68030k"
# elif defined(__mc68040__)
#  define NO_ARCH "68040k"
# elif defined(__mc68060__)
#  define NO_ARCH "68060k"
# else
#  define NO_ARCH "68k"
# endif
# define NO_BIG_ENDIAN 1
# define NO_IS_68K 1
#endif

#if defined(__mips__) || defined(mips) || defined(__mips) || defined(__MIPS__)
# define NO_BI_ENDIAN 1
# if defined(__MIPS_ISA3__) || defined(__MIPS_ISA4__) || defined(_R4000) || defined(_MIPS_ISA_MIPS3) || defined(_MIPS_ISA_MIPS4) || __mips >= 3
#  define NO_NATIVE_64_BIT 1
# else
#  define NO_NATIVE_32_BIT 1
# endif
# define NO_CPU "Mips"
# if defined(_MIPS_ISA_MIPS1) || defined(_R3000) || __mips == 1
#  define NO_ARCH "Mips1"
# elif defined(_MIPS_ISA_MIPS2) || defined(__MIPS_ISA2__) || __mips == 2
#  define NO_ARCH "Mips2"
# elif defined(_MIPS_ISA_MIPS3) || defined(__MIPS_ISA3__) || __mips == 3
#  define NO_ARCH "Mips3"
# elif defined(_MIPS_ISA_MIPS4) || defined(__MIPS_ISA4__) || __mips == 4
#  define NO_ARCH "Mips4"
# else
#  define NO_ARCH "Mips"
# endif
# if defined(_MIPSEB) || defined(__MIPSEB) || defined(__MIPSEB__)
#  define NO_BIG_ENDIAN 1
# else
#  define NO_LITTLE_ENDIAN 1
# endif
# define NO_IS_MIPS 1
#endif

#if defined(__powerpc) || defined(__powerpc__) || defined(__powerpc64__) || defined(__POWERPC__) || defined(__ppc__) || defined(__PPC__) || defined(__PPC64__) || defined(_ARCH_PPC) || defined(_M_PPC) || defined(_ARCH_PPC) || defined(_ARCH_PPC64) || defined(__PPCGECKO__) || defined(__PPCBROADWAY__) || defined(_XENON) || defined(__ppc)
# define NO_CPU "PowerPc"
# if defined(__powerpc64__) || defined(__PPC64__) || defined(_ARCH_PPC64) || defined(_XENON)
#  define NO_NATIVE_64_BIT 1
# else
#  define NO_NATIVE_32_BIT 1
# endif
# if defined(_ARCH_440)
#  define NO_ARCH "PPC_440"
# elif defined(_ARCH_450)
#  define NO_ARCH "PPC_450"
# elif defined(_ARCH_601) || defined(__ppc601__) || _M_PPC == 601
#  define NO_ARCH "PPC_601"
# elif defined(_ARCH_603) || defined(__ppc603__) || _M_PPC == 603
#  define NO_ARCH "PPC_603"
# elif defined(_ARCH_604) || defined(__ppc604__) || _M_PPC == 604
#  define NO_ARCH "PPC_604"
# elif _M_PPC == 620
#  define NO_ARCH "PPC_620"
# else
#  define NO_ARCH "PPC"
# endif
# define NO_BIG_ENDIAN 1
# define NO_IS_PPC 1
#endif

#if defined(pyr)
# define NO_NATIVE_32_BIT 1
# define NO_CPU "Pyramid"
# define NO_ARCH "PYRAMID_9810"
# define NO_BIG_ENDIAN 1
# define NO_IS_PYR 1
#endif

#if defined(__THW_RS6000) || defined(_IBMR2) || defined(_POWER) || defined(_ARCH_PWR) || defined(_ARCH_PWR2) || defined(_ARCH_PWR3) || defined(_ARCH_PWR4)
# define NO_CPU "Power"
# if defined(_ARCH_PWR3) || defined(_ARCH_PWR4)
#  define NO_NATIVE_64_BIT 1
# else
#  define NO_NATIVE_32_BIT 1
# endif

# if defined(__THW_RS6000)
#  define NO_ARCH "RS/6000"
# elif defined(_IBMR2) || defined(_ARCH_PWR2)
#  define NO_ARCH "Power2"
# elif defined(_ARCH_PWR3)
#  define NO_ARCH "Power3"
# elif defined(_ARCH_PWR4)
#  define NO_ARCH "Power4"
# elif defined(_POWER) || defined(_ARCH_PWR)
#  define NO_ARCH "Power1"
# else
#  define NO_ARCH "Power"
# endif
# define NO_BIG_ENDIAN 1
# define NO_IS_POWER 1
#endif

#if defined(__sparc__) || defined(__sparc)
# if defined(__sparc_v9__) || defined(__sparcv9)
#  define NO_BI_ENDIAN 1
#  define NO_NATIVE_64_BIT 1
#  define NO_ARCH "UltraSparc"
# else
#  define NO_NATIVE_32_BIT 1
#  define NO_ARCH "SuperSparc"
# endif
# define NO_BIG_ENDIAN 1
# define NO_CPU "Sparc"
# define NO_IS_SPARC 1
#endif

#if defined(__sh__)
# define NO_SELECTABLE_ENDIAN 1
# if defined(__SH5__)
#  define NO_NATIVE_64_BIT 1
# else
#  define NO_NATIVE_32_BIT 1
# endif
# if defined(__sh3__) || defined(__SH3__) || defined(__SH4__) || defined(__SH5__)
#  define NO_BI_ENDIAN 1
# endif
# define NO_CPU "SuperH"
# if defined(__sh1__)
#  define NO_ARCH "sh1"
# elif defined(__sh2__)
#  define NO_ARCH "sh2"
# elif defined(__sh3__) || defined(__SH3__)
#  define NO_ARCH "sh3"
# elif defined(__SH4__)
#  define NO_ARCH "sh4"
# elif defined(__SH5__)
#  define NO_ARCH "sh5"
# else
#  define NO_ARCH "sh"
# endif
# define NO_LITTLE_ENDIAN 1
# define NO_IS_SH 1
#endif

#if defined(__370__) || defined(__THW_370__) || defined(__s390__) || defined(__s390x__) || defined(__zarch__) || defined(__SYSC_ZARCH__)
# if defined(__s390x__) || defined(__zarch__) || defined(__SYSC_ZARCH__)
#  define NO_NATIVE_64_BIT 1
# else
#  define NO_NATIVE_32_BIT 1
# endif
# define NO_CPU "SystemZ"
# if defined(__370__) || defined(__THW_370__)
#  define NO_ARCH "System/370"
# elif defined(__s390__)
#  define NO_ARCH "System/390"
# elif defined(__s390x__) || defined(__zarch__) || defined(__SYSC_ZARCH__)
#  define NO_ARCH "z/Architecture"
# else
#  define NO_ARCH "System/Z"
# endif
# define NO_BIG_ENDIAN 1
# define NO_IS_SYSTEMZ 1
#endif

#ifdef NO_BIG_ENDIAN
# define NO_BE_CHECK 1
# define NO_LE_CHECK 0
#else
# define NO_BE_CHECK 0
# define NO_LE_CHECK 1
#endif

#if defined(NO_NATIVE_32_BIT)
# define NO_ARCH_BIT "32"
#elif defined(NO_NATIVE_64_BIT)
# define NO_ARCH_BIT "64"
#elif defined(NO_NATIVE_16_BIT)
# define NO_ARCH_BIT "16"
#else
# define NO_ARCH_BIT "UNKNOWN"
#endif

#endif /* NO_ONE_CPU_BASIC_H */
