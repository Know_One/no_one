# No_One's repo #

This is collection of simple, easy to include libraries. They are intended to be embedded directly into your projects. But if needed you could build them as an external library.

Most the libraries will be C. Although I may add some c++ versions as well.

### Current Projects ###

* Random number library. ( no_random.c no_random.h no_test_random.c no_random_asm_gas_x86_64.s no_random_asm_gas_x86.s )
* CPU info helper header. ( no_cpu_basic.h )

### No_One's random library ###

This library is used to generate random numbers. It does this by extracting entropy from the system clock, and then scrambling the data using hashing algorithms to remove bias.

* Check header for details on usage.
* Only no_random.c no_random.h are required. The rest are for optimizing, or example.
* no_test_random.c is an optional file.
* no_random_asm_gas_x86_64.s is an optional file. 
* no_random_asm_gas_x86.s is an optional file.
* No external dependencies are required. 
* Creative Commons License. Allows for private, and commercial use.

### No_One's cpu info helper ###

Uses a mess of if/def preprocessor checks to gather information on your processor. 
This allows you to do simple compile time checks. You can check for endian,cpu,bit size, etc...
It also provides some vanity strings for the cpu/arch.


### Contact ###

* Repo is owned by No_One (Know_One username on bitbucket)
