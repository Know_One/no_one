.file   "no_random_asm_gas_x86_64.s"
.text

.globl time_eo
.type  time_eo, @function
time_eo:
	rdtsc             # Get cpu clock time
	movq  %rax, %r8   # Cache time stamp on r8
	pause
	rdtsc             # Get cpu clock time
	xorq  %r8,  %rax  # Remove difference from rax
	andq  $1,   %rax  # & rax with 1
	ret               # Return

.globl no_whitened_bita
.type  no_whitened_bita, @function
no_whitened_bita:
	rdtsc                       # Get cpu clock time
	movq   %rax, %r8            # Cache lower in r8
	andq   $1,   %r8            # & r8 with 1
	pause
	rdtsc                       # Get cpu clock time
	movq   %rax, %r9            # Cache lower in r9
	andq   $1,   %r9            # & r9 with 1
	cmpq   %r9,  %r8            # if( r9 == r8 )
	je     no_whitened_bita     # Then continue loop
	cmpq   $1,   %r8            # if( r8 == 1 )
	je    .no_whitened_bit_sub1 # Then do no_cpu_time_sub1
	movq   $1,   %rax           # Else set return value to 1
	jmp   .no_whitened_bit_sub2 # goto return
.no_whitened_bit_sub1:
	movq   $0x0, %rax           # Set return value to 0
.no_whitened_bit_sub2:
	ret                         # Return

.globl no_fill_buffer
.type  no_fill_buffer, @function
no_fill_buffer:
	movq   %rsi,  %rax         # Store size in the divisor
	movq   $8,    %r12         # Store size of block in r12
	xorq   %rdx,  %rdx         # rdx = 0
	divq   %r12                # rax = rax ÷ r12 (rdx = remainder)
	salq   $3,    %rdx         # Convert rdx from bytes to bits (rdx * 8)
	xorq   %r9,   %r9          # r9 = 0 (Clear random data acculator)
	cmpq   $0x0,  %rax         # if( rax != 0 )
	jne   .no_fill_buffer_sub1 # Then goto .no_fill_buffer_sub1
	movq   %rdx,  %r13         # Put bit count into r13
	movq   $1,    %r12         # r12 == 1 We still have data to write
	xorq   %r10,  %r10         # r10 = 0 (No left over bits)
	jmp   .no_fill_buffer_sub3 # Skip multi block stuff
.no_fill_buffer_sub1:
	movq   %rax,  %r12         # r12 = rax
	movq   %rdx,  %r10         # Store size of block in r10
.no_fill_buffer_sub2:
	movq   $64,   %r13         # r13 = 64
.no_fill_buffer_sub3:
	cmpq   $0x0,  %r13         # if( r13 == 0 )
	je    .no_fill_buffer_sub4 # Then break loop
	jmp    whitened_bit        # Call whitened_bit
.no_fill_buffer_sub4:
	xorq   %r9,  (%rdi)        # Copy data into buffer. (Xor over old)
	xorq   %r9,   %r9          # r9 = 0
	leaq 8(%rdi), %rdi         # Buffer position ++
	subq   $1,    %r12         # r12 -= 1
	cmpq   $0x0,  %r12         # if( r12 != 0 )
	jne   .no_fill_buffer_sub2 # then goto .no_fill_buffer_sub2
	movq   %r10,  %r13         # Set counter again
	xorq   %r10,  %r10         # r10 = 0
	movq   $1,    %r12         # r12 == 1 We MAY still have data to write
	cmpq   $0x0,  %r13         # if( r13 != 0 )
	jne   .no_fill_buffer_sub3 # then goto .no_fill_buffer_sub3
	ret                        # Return

whitened_bit:
	rdtsc                   # Get cpu clock time
	movq  %rax,  %r8        # Cache lower in r8
	andq  $1,    %r8        # & r8 with 1
	nop                     # Pause or nop. Higher chance for entropy.
	nop                     # Pause or nop. Higher chance for entropy.
	nop                     # Pause or nop. Higher chance for entropy. (3 times seesm to be the sweet spot)
	rdtsc                   # Get cpu clock time
	movq  %rax,  %r11       # Cache lower in r8
	andq  $1,    %r11       # & rax with 1
	cmpq  %r11,  %r8        # if( rax == r8 )
	je    whitened_bit      # Then continue loop
	cmpq  $1,    %r8        # if( r8 == 1 )
	je   .whitened_bit_sub1 # Then do no_cpu_time_sub1
	movq  $1,    %rax       # Else set bit value to 1
	jmp  .whitened_bit_sub2 # goto return
.whitened_bit_sub1:
	movq  $0x0,  %rax       # Set bit value to 0
.whitened_bit_sub2:
	salq  $1,    %r9        # Shift buffer(r9) by one
	orq   %rax,  %r9        # add bit to buffer(r9)
	subq  $1,    %r13       # r13 -= 1
	jmp  .no_fill_buffer_sub3
