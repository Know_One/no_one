.file   "no_random_asm_gas_x86.s"
.text

.globl time_eo
.type  time_eo, @function
time_eo:
	rdtsc             # Get cpu clock time
	movl  %eax,  %ebx # Cache time stamp on ebx
	nop               # Pause or nop. Higher chance for entropy.
	rdtsc             # Get cpu clock time
	xorl  %ebx,  %eax # Remove difference from eax
	andl  $1,    %eax # & eax with 1
	ret              # Return

.globl no_whitened_bita
.type  no_whitened_bita, @function
no_whitened_bita:
	rdtsc                       # Get cpu clock time
	movl   %eax,  %ebx          # Cache lower in ebx
	andl   $1,    %ebx          # & ebx with 1
	nop                         # Pause or nop. Higher chance for entropy.
	rdtsc                       # Get cpu clock time
	movl   %eax,  %ecx          # Cache lower in ecx
	andl   $1,    %ecx          # & ecx with 1
	cmpl   %ecx,  %ebx          # if( ecx == ebx )
	je     no_whitened_bita     # Then continue loop
	cmpl   $1,    %ebx          # if( ebx == 1 )
	je    .no_whitened_bit_sub1 # Then do no_cpu_time_sub1
	movl   $1,    %eax          # Else set return value to 1
	jmp   .no_whitened_bit_sub2 # goto return
.no_whitened_bit_sub1:
	movl   $0x0,  %eax          # Set return value to 0
.no_whitened_bit_sub2:
	ret                        # Return

.globl no_fill_buffer
.type  no_fill_buffer, @function
no_fill_buffer:
	pushl   %ebp                # Store stack pointer
	movl    %esp,   %ebp        # Find postion in stack
	movd  8(%ebp),  %mm1        # Store buffer pointer
	movl 12(%ebp),  %eax        # Store size in the divisor
	xorl    %edx,   %edx        # edx = 0
	movl    $4,     %ebx        # ebx = 4 (To convert 8 bit blocks to 32 bit)
	divl    %ebx                # eax = eax ÷ ebx (edx = remainder)
	sall    $3,     %edx        # Convert edx from bytes to bits (edx * 8)
	xorl    %ecx,   %ecx        # ecx = 0 (Clear random data acculator)
	cmpl    $0x0,   %eax        # if( eax != 0 )
	jne    .no_fill_buffer_sub1 # Then goto .no_fill_buffer_sub1
	movl    %edx,   %edi        # Put bit count into edi
	movl    $1,     %ebx        # ebx == 1 We still have data to write
	xorl    %esi,   %esi        # esi = 0 (No left over bits)
	jmp    .no_fill_buffer_sub3 # Skip multi block stuff
.no_fill_buffer_sub1:
	movl    %eax,   %ebx        # ebx = eax
	movl    %edx,   %esi        # Store size of block in esi
.no_fill_buffer_sub2:
	movl    $32,    %edi        # edi = 32
.no_fill_buffer_sub3:
	cmpl    $0x0,   %edi        # if( edi == 0 )
	je     .no_fill_buffer_sub4 # Then break loop
	jmp     whitened_bit        # Call whitened_bit
.no_fill_buffer_sub4:
	movd    %mm1,   %eax        # Uncache buffer pointer
	xorl    %ecx,  (%eax)       # Copy data into buffer. (Xor over old)
	xorl    %ecx,   %ecx        # ecx = 0 (Clear random data acculator)
	leal  4(%eax),  %eax        # Buffer position ++
	movd    %eax,   %mm1        # Cache buffer pointer
	subl    $1,     %ebx        # ebx -= 1 (Bit counter)
	cmpl    $0x0,   %ebx        # if( ebx != 0 )
	jne    .no_fill_buffer_sub2 # then goto .no_fill_buffer_sub2
	movl    %esi,   %edi        # Set counter again
	xorl    %esi,   %esi        # esi = 0
	movl    $1,     %ebx        # ebx == 1 We MAY still have data to write
	cmpl    $0x0,   %edi        # if( esi != 0 )
	jne    .no_fill_buffer_sub3 # then goto .no_fill_buffer_sub3
	emms                        # Restore floating point registers
	popl    %ebp                # Restore stack
	ret                        # Return
	
whitened_bit:
	rdtsc                   # Get cpu clock time
	andl  $1,    %eax       # & eax with 1
	movd  %eax,  %mm0       # Cache lower in mm0
	nop                     # Pause or nop. Higher chance for entropy.
	nop                     # Pause or nop. Higher chance for entropy.
	nop                     # Pause or nop. Higher chance for entropy. (3 times seesm to be the sweet spot)
	rdtsc                   # Get cpu clock time
	andl  $1,    %eax       # & eax with 1
	movd  %mm0,  %edx       # Uncache lower from mm0
	cmpl  %eax,  %edx       # if( eax == edx )
	je    whitened_bit      # Then continue loop
	cmpl  $1,    %edx       # if( edx == 1 )
	je   .whitened_bit_sub1 # Then do no_cpu_time_sub1
	movl  $1,    %eax       # Else bit return value to 1
	jmp  .whitened_bit_sub2 # goto return
.whitened_bit_sub1:
	movl  $0x0,  %eax       # Set bit value to 0
.whitened_bit_sub2:
	sall  $1,     %ecx      # Shift buffer(mm0) by one
	orl   %eax,   %ecx      # Add bit to buffer(mm0)
	subl  $1,     %edi      # edi -= 1
	jmp  .no_fill_buffer_sub3
